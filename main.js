// ---------------------------------------------------------------------------------------------------------- //
// [ INIT ]                                                                                                   //
// ---------------------------------------------------------------------------------------------------------- //

const express = require('express');
const app = express();
const port = 1555;
const FUNCTIONS = require('./functions');

// ---------------------------------------------------------------------------------------------------------- //
// [ ROUTES ]                                                                                                 //
// ---------------------------------------------------------------------------------------------------------- //

app.get('/jobs/:id?', (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    FUNCTIONS.getJobs((data) => {
        res.send(data);
    }, req.params.id);
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})