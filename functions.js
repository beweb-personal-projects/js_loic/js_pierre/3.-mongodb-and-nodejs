// ---------------------------------------------------------------------------------------------------------- //
// [ INIT ]                                                                                                   //
// ---------------------------------------------------------------------------------------------------------- //

const MongoClient = require('mongodb').MongoClient;

function connectDB(cb) {
    const DB_URL =  "mongodb://172.17.0.2/HOSPITAL";

    MongoClient.connect(DB_URL, function(err, db) {
        if (err) throw err;
        let dbo = db.db("HOSPITAL");
        cb(dbo, db);
    });
}

// ---------------------------------------------------------------------------------------------------------- //
// [ MODULES ]                                                                                                //
// ---------------------------------------------------------------------------------------------------------- //

module.exports = {
    getJobs: (cb, id) => {
        connectDB((dbo, db) => {
            if(id) {
                dbo.collection("JOBS").find({ job_identifier: Number(id) }).toArray(function(err, job) {
                    if (err) throw err;
                    cb(job);
                    db.close();
                });
            } else {
                dbo.collection("JOBS").find().toArray(function(err, jobs) {
                    if (err) throw err;
                    cb(jobs);
                    db.close();
                });
            }
        });
    }
}